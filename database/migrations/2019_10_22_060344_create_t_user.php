<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_user', function (Blueprint $table) {
            $table->bigIncrements('id_user');
            $table->string('nama_lengkap', 100);
            $table->string('email', 100);
            $table->string('password', 100);
            $table->string('jenis_kelamin', 11);
            $table->string('universitas', 100);
            $table->string('fakultas', 100);
            $table->string('achievement', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_user');
    }
}
