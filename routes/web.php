<?php

// use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', 'IndexController@index');

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/profile', function () {
    return view('/profile');
});

Route::get('/jadwal', function () {
    return view('jadwal');
});

Route::get('/task', function () {
    return view('task');
});

Route::get('/calendar', function () {
    return view('calendar');
});

Route::get('/profile', 'UserController@profile');
Route::put('/profile', 'UserController@update');

Route::get('/index', 'IndexController@index');
Route::get('/jadwal', 'JadwalController@index');
Route::get('/jadwal/create', 'JadwalController@create');
Route::post('/jadwal', 'JadwalController@store');
Route::get('/jadwal/{id}/edit', 'JadwalController@edit');
Route::put('/jadwal/{id}', 'JadwalController@update');
Route::delete('/jadwal/{id}', 'JadwalController@destroy');
Route::delete('/jadwal', 'JadwalController@currentDate');

// AdminPiket
Route::middleware('auth', 'checkadmin')->group(function(){

    Route::get('/admin_index', 'AdminPiketController@index');
    Route::get('/admin_piket/create', 'AdminPiketController@create');
    Route::post('/admin_piket', 'AdminPiketController@store');
    Route::get('/admin_piket/{id}/edit', 'AdminPiketController@edit');
    Route::put('/admin_piket/{id}', 'AdminPiketController@update');
    
    Route::delete('/admin_delete/{id}', 'AdminPiketController@destroy');
    Route::get('/admin_edit/{id}/edit', 'AdminPiketController@edit_piket');
    Route::put('/admin_edit/{id}', 'AdminPiketController@update_piket');
    
});

Route::get('/task', 'TaskController@index');
Route::get('/task/create', 'TaskController@create');
Route::post('/task', 'TaskController@store');
Route::get('/task/{edit}/edit', 'TaskController@edit');
Route::put('/task/{id}', 'TaskController@update');
Route::delete('/task/{id}', 'TaskController@destroy');
Route::get('/task/show', 'TaskController@showtask');

Route::get('/fullcalendareventmaster', 'FullcalendarController@index');
Route::post('/fullcalendareventmaster/create', 'FullcalendarController@create');
Route::post('/fullcalendareventmaster/update', 'FullcalendarController@update');
Route::post('/fullcalendareventmaster/delete', 'FullcalendarController@destroy');

Auth::routes();

Route::get('/index', 'IndexController@index')->name('index');
Route::get('/logout', 'LoginController@logout');
