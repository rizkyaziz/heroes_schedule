<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piket extends Model
{
   protected $table = 'piket';
   protected $fillable = ['nama', 'hari', 'tugas'];
   
}
