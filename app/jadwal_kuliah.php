<?php 
	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class Kadwal_Kuliah extends Model {

		public $table = 'jadwal_kuliah';

        protected $fillable = ['id', 'hari', 'jam_masuk', 'jam_selesai', 'mata_kuliah', 'sks', 'dosen', 'ruangan'];
        // protected $primaryKey = 'id_jadwal_kuliah';
	}
?>