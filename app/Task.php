<?php 
	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class Task extends Model {

		public $table = 'task';

		protected $fillable = ['id', 'name', 'type', 'description', 'media', 'deadline'];
		
		public function getCreatedAttribute() {
			return \Carbon\Carbon::parse($this->attributes['created_at'])->format('d, M Y H:i');
		}
	}
?>