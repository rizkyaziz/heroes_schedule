<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Piket;

class AdminPiketController extends Controller
{
    public function index() {        
        $data['piket_senin'] = \DB::table('piket')->where('hari', 'senin')->get();
        $data['piket_selasa'] = \DB::table('piket')->where('hari', 'selasa')->get();
        $data['piket_rabu'] = \DB::table('piket')->where('hari', 'rabu')->get();
        $data['piket_kamis'] = \DB::table('piket')->where('hari', 'kamis')->get();
        $data['piket_jumat'] = \DB::table('piket')->where('hari', 'jumat')->get();

        $senen = \DB::table('piket')->where('hari', 'senin')->count();
        $selasa = \DB::table('piket')->where('hari', 'selasa')->count();
        $rabu = \DB::table('piket')->where('hari', 'rabu')->count();
        $kamis = \DB::table('piket')->where('hari', 'kamis')->count();
        $jumat = \DB::table('piket')->where('hari', 'jumat')->count();

        if ($senen > $selasa && $senen > $rabu && $senen > $kamis && $senen > $jumat) {
            $data['isset'] = \DB::table('piket')->where('hari', 'senin')->get();
        }
        elseif ($selasa > $senen && $selasa > $rabu && $selasa > $kamis && $selasa > $jumat) {
            $data['isset'] = \DB::table('piket')->where('hari', 'selasa')->get();
        }
        elseif ($rabu > $senen && $rabu > $selasa && $rabu > $kamis && $rabu > $jumat) {
            $data['isset'] = \DB::table('piket')->where('hari', 'rabu')->get();
        }
        elseif ($kamis > $senen && $kamis > $selasa && $kamis > $rabu && $kamis > $jumat) {
            $data['isset'] = \DB::table('piket')->where('hari', 'kamis')->get();
        }        
        else {
            $data['isset'] = \DB::table('piket')->where('hari', 'jumat')->get();
        }
        
        
        $piket = \DB::table('piket')->get();
        return view('admin.admin_piket', $data);
    }

    public function create () {
        return view('Form_Piket');
    }

    public function store(Request $request) {

        $rule = [
        'hari' => 'required|string',
        // 'minggu' => 'required',
    ];
    $this->validate($request, $rule);

        // $input = $request->all();
        // unset($input['_token']);
        // $status = \DB::table('piket')->insert($input);

        Piket::create([
            'nama' => $request->nama1,
            'hari' => $request->hari,
        ]);

        if ($request->nama2 != null) {
            Piket::create([
                'nama' => $request->nama2,
                'hari' => $request->hari,
            ]);
        }

        if ($request->nama3 != null) {
            Piket::create([
                'nama' => $request->nama3,
                'hari' => $request->hari,
            ]);
        }

        if ($request->nama4 != null) {
            Piket::create([
                'nama' => $request->nama4,
                'hari' => $request->hari,
            ]);
        }
        
        if ($request->nama5 != null) {
            Piket::create([
                'nama' => $request->nama5,
                'hari' => $request->hari,
            ]);
        }

        if ($request->nama6 != null) {
            Piket::create([
                'nama' => $request->nama6,
                'hari' => $request->hari,
            ]);
        }

        // if ($status) {
            return redirect('/admin_index')->with('success', 'Data berhasil ditambahkan');
        // } else {
        //     return('/admin_index/create')->with('error', 'Data gagal ditambahkan');
        // }
    }

    public function edit(Request $request, $id) {            
        $data['piket'] = \DB::table('piket')->find($id);
        return view('Form_Piket', $data);
    }

    public function edit_piket(Request $request, $id) {            
        $data['jadwal_piket'] = \DB::table('piket')->find($id);
        return view('admin.admin_edit', $data);
    }

    public function update(Request $request, $id) {
        
        
        $rule = [
        'hari' => 'required|string',
        // 'minggu' => 'required',
        ];
        $this->validate($request, $rule);

        // $input = $request->all();
        // unset($input['_token']);
        // unset($input['_method']);

        // $status = \DB::table('piket')->where('id', $id)->update($input);
        // $matkul = \App\jadwal_kuliah::find($id);

        Piket::where('id', $id)->update([
            'nama' => $request->nama,
            'tugas' => $request->tugas,
            'hari' => $request->hari,
        ]);

        // if ($status) {
            return redirect('/admin_index')->with('success', 'Data berhasil diubah');
        // } else {
        //     return('/admin_index/create')->with('error', 'Data gagal ditambahkan');
        // }
    }

    public function update_piket(Request $request, $id) {
        
        
        $rule = [
        'hari' => 'required|string',
        // 'minggu' => 'required',
    ];
    $this->validate($request, $rule);

        $input = $request->all();
        unset($input['_token']);
        unset($input['_method']);

        $status = \DB::table('piket')->where('id', $id)->update($input);
        // $matkul = \App\jadwal_kuliah::find($id);

        if ($status) {
            return redirect('/admin_index')->with('success', 'Data berhasil diubah');
        } else {
            return('/admin_index/create')->with('error', 'Data gagal ditambahkan');
        }
    }

    public function destroy(Request $request, $id) {
        
        $status = \DB::table('piket')->where('id', $id)->delete();
        

        if ($status) {
            return redirect('/admin_index')->with('success', 'Data berhasil dihapus');
        } else {
            return redirect('/admin_index')->with('error', 'Data gagal dihapus');
        }
    }
}
