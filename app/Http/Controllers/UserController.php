<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

class UserController extends Controller
{
    public function index() {
        $data['users'] = \DB::table('users')->get();
        return view('index', $data);
    }

    public function profile() {
        $profile['profile'] = \DB::table('users')->get();

        return view('profile', $profile);
    }

    public function update(Request $request) {    

        $input = $request->all();
        unset($input['_token']);
        unset($input['_method']);

        if($request->hasfile('image'))
        {
            $now = strtotime(Carbon::now());
            $filename = $now . '.' . $request->file('image')->getClientOriginalExtension();
            $store = $request->media->storeAs('public/foto/users', $filename);
            $input['media'] = $filename;
                        
        }
            
        $status = $profile->update($input);

        if ($status) {
            return redirect('/profile')->with('success', 'Foto berhasil diubah');
        } else {
            return('/profile')->with('error', 'Foto gagal ditambahkan');
        }
    }
}
