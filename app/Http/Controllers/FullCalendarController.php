<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events;

class FullCalendarController extends Controller
{
    public function index()
    {
        if(request()->ajax()) 
        {
 
         $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
         $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');
 
         $data = Events::whereDate('start', '=>', $start)->whereDate('end',   '=>', $end)->get(['id','title','start', 'end']);
         return Response::json($data);
        }
        return view('fullcalendar');
    }
    
   
    public function create(Request $request)
    {  
        $insert = [ 'title' => $request->title,
                       'start' => $request->start,
                       'end' => $request->end
                    ];
        $event = \App\Events::insert($insert);
        return Response::json($event);
    }

    public function store() {
        $rule = [
        'title' => 'required|string',
        'start' => 'required',
        'end' => 'required',
    ];
    $this->validate($request, $rule);

        $input = $request->all();
        unset($input['_token']);
        $status = \DB::table('_events')->insert($input);
        // $status = \App\jadwal_kuliah::create($input);

        if ($status) {
            return redirect('/fullcalendar')->with('success', 'Data berhasil ditambahkan');
        } else {
            return('/fullcalendar')->with('error', 'Data gagal ditambahkan');
        }
    }
     
 
    public function update(Request $request)
    {   
        $where = array('id' => $request->id);
        $updateArr = ['title' => $request->title,'start' => $request->start, 'end' => $request->end];
        $event  = Events::where($where)->update($updateArr);
 
        return Response::json($event);
    } 
 
 
    public function destroy(Request $request)
    {
        $events = \App\Events::find($id);
        $event = $events->delete();
        // $event = Events::where('id',$request->id)->delete();
   
        return Response::json($event);
    }
}
