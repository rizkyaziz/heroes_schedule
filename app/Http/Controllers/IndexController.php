<?php

namespace App\Http\Controllers;
use App\Jadwal_kuliah;
use Illuminate\Http\Request;
use Carbon\Carbon;

class IndexController extends Controller
{
    public function index() {        
        $data['jadwal_kuliah'] = \DB::table('jadwal_kuliah')->orderBy('hari', 'desc' )->get();
        
        $data['jadwal_piket'] = \DB::table('piket')->get();
        
        $data['piket_senin'] = \DB::table('piket')->where('hari', 'senin')->get();
        $data['piket_selasa'] = \DB::table('piket')->where('hari', 'like', '%'.'selasa'.'%')->get();
        $data['piket_rabu'] = \DB::table('piket')->where('hari', 'like', '%'.'rabu'.'%')->get();
        $data['piket_kamis'] = \DB::table('piket')->where('hari', 'like', '%'.'kamis'.'%')->get();
        $data['piket_jumat'] = \DB::table('piket')->where('hari', 'like', '%'.'jumat'.'%')->get();

        $data['kuliah'] = \DB::table('task')->where('type', 'like', '%'. 'kuliah'.'%')->get();
        $data['kerja'] = \DB::table('task')->where('type', 'like', '%'. 'kerja'.'%')->get();
        $data['task'] = \DB::table('task')->orderBy('deadline', 'asc')->get();
                
        $now = Carbon::now()->translatedFormat('l');
        $data['filter'] = \DB::table('jadwal_kuliah')->where('hari', 'like', $now)->get();
        $data['filter_piket'] = \DB::table('piket')->where('hari', 'like', $now)->get();        

        $data['filter_hari'] = \DB::table('piket')->where('hari', 'Sabtu')->get();
        $data['filter_hari'] = \DB::table('piket')->where('hari', 'Minggu')->get();
                        
        return view('index', $data);
    }

    public function count() {
        $count = \DB::table('task')->where('type', 'like', '%' .'kuliah'. '%')->count();
        return view('index',$count);
    }

    public function currentDate() {
        $now = strtotime(Carbon::now());
        $display['filter'] = \DB::table('task')->where('hari', '=', $now)->get();
        return view('index', $display);
    }

    public function filter() {

        $now = Carbon::now()->translatedFormat('l');
        $status['filter'] = \DB::table('jadwal_kuliah')->where('hari', 'like', $now)->get();
        $status['filter_piket'] = \DB::table('jadwal_piket')->where('hari', 'like', $now)->get();

        $random['random_user'] = \DB::table('jadwal_kuliah')->where('anggota_1', 'anggota_2', 'anggota_3', 'like', $now)->inRandom()->get();
        dd($random);

        return view('index', $status);
    }

}
