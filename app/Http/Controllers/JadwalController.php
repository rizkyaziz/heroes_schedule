<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JadwalController extends Controller
{
    public function index() {
        $data['jadwal_kuliah'] = \DB::table('jadwal_kuliah')->orderBy('hari', 'desc')->get();
        
        $data['piket_senin'] = \DB::table('piket')->where('hari', 'senin')->get();
        $data['piket_selasa'] = \DB::table('piket')->where('hari', 'like', '%'.'selasa'.'%')->get();
        $data['piket_rabu'] = \DB::table('piket')->where('hari', 'like', '%'.'rabu'.'%')->get();
        $data['piket_kamis'] = \DB::table('piket')->where('hari', 'like', '%'.'kamis'.'%')->get();
        $data['piket_jumat'] = \DB::table('piket')->where('hari', 'like', '%'.'jumat'.'%')->get();
                           
        return view('jadwal', $data);
    }

    
    public function create () {
        return view('Form_Kuliah');
    }
    // public function create_piket () {
    //     return view('Form_Piket');
    // }

    public function store(Request $request) {

        $rule = [
        'hari' => 'required|string',
        'jam_masuk' => 'required',
        'jam_selesai' => 'required',
        'mata_kuliah' => 'required',
        'sks' => 'required|numeric',
        'dosen' => 'required',
        'ruangan' => 'required', 
    ];
    $this->validate($request, $rule);

        $input = $request->all();
        unset($input['_token']);
       
        $status = \DB::table('jadwal_kuliah')->insert($input);


        // $status = \App\jadwal_kuliah::create($input);

        if ($status) {
            return redirect('/jadwal')->with('success', 'Data berhasil ditambahkan');
        } else {
            return('/jadwal/create')->with('error', 'Data gagal ditambahkan');
        }
    }    

    public function edit(Request $request, $id) {
        $data['jadwal_kuliah'] = \DB::table('jadwal_kuliah')->find($id);
        return view('Form_Kuliah', $data);
    }    

    public function update(Request $request, $id) {
        $rule = [
        'hari' => 'required|string',
        'jam_masuk' => 'required',
        'jam_selesai' => 'required',
        'mata_kuliah' => 'required',
        'sks' => 'required|numeric',
        'dosen' => 'required',
        'ruangan' => 'required',
    ];
    $this->validate($request, $rule);

        $input = $request->all();
        unset($input['_token']);
        unset($input['_method']);

        $status = \DB::table('jadwal_kuliah')->where('id', $id)->update($input);
        // $matkul = \App\jadwal_kuliah::find($id);
        // // $status = $matkul->update($input);

        if ($status) {
            return redirect('/jadwal')->with('success', 'Data berhasil diubah');
        } else {
            return redirect('/jadwal')->with('error', 'Tidak ada data yang diubah');
        }
    }
    
    public function destroy(Request $request, $id) {
        // $kuliah = \App\jadwal_kuliah::find($id);
        // $status = $kuliah->delete();
        $status = \DB::table('jadwal_kuliah')->where('id', $id)->delete();

        if ($status) {
            return redirect('/jadwal')->with('success', 'Data berhasil dihapus');
        } else {
            return redirect('/jadwal/create')->with('error', 'Data gagal dihapus');
        }
    }    
}
