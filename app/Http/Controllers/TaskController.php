<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Task;
use Carbon\Carbon;

class TaskController extends Controller
{
    public function index () {
        $data['task'] = \DB::table('task')->get();
        $data['kuliah'] = \DB::table('task')->where('type', 'like', '%'. 'kuliah'.'%')->orderBy('deadline', 'asc')->get();
        $data['kerja'] = \DB::table('task')->where('type', 'like', '%'. 'kerja'. '%')->get();
        
        // $now = dd(Carbon::setLocale('id'));
        
        

        return view('task', $data);
    }

    // public function show(Request $request) {
    //     $show = \DB::table('task')->find($id);        
    //     return view('task', $show);
    // }

    public function create() {
        return view('Form_Task');
    }

    public function store(Request $request) {
        $rule = [
        'name' => 'required|string',
        'type' => 'required',
        'deadline' => 'required',           
    ];
    $this->validate($request, $rule);
        $input = $request->all();
        unset($input['_token']);      

        // $status = \App\jadwal_kuliah::create($input);

        // $matkul = new \App\jadwal_kuliah;
        // $matkul->hari = $input['hari'];
        // $matkul->jam_masuk = $input['jam_masuk'];
      
        if($request->hasfile('media'))
        {
            $now = strtotime(Carbon::now());
            $filename = $now . '.' . $request->file('media')->getClientOriginalExtension();
            $store = $request->media->storeAs('public/task', $filename);
            $input['media'] = $filename;
        }

        $status = Task::create($input);

        if ($status) {
            return redirect('/task')->with('success', 'Data berhasil ditambahkan');
        } else {
            return('/task/create')->with('error', 'Data gagal ditambahkan');
        }
    }

    public function edit(Request $request, $id) {
        $data['task'] = \DB::table('task')->find($id);
        return view('Form_Task', $data);
    }

    public function update(Request $request, $id) {        

        $rule = [
        'name' => 'required|string',        
        'type' => 'required',
        'deadline' => 'required',        
        
    ];
    $this->validate($request, $rule);

        $input = $request->all();
        unset($input['_token']);
        unset($input['_method']);

        if($request->hasfile('media'))
        {
            $now = strtotime(Carbon::now());
            $filename = $now . '.' . $request->file('media')->getClientOriginalExtension();
            $store = $request->media->storeAs('public/task', $filename);
            $input['media'] = $filename;
        }

        // $status = Task::update($input);
        $task = \App\Task::find($id);
        $status = $task->update($input);        

        if ($status) {
            return redirect('/task')->with('success', 'Data berhasil diubah');
        } else {
            return('/task/create')->with('error', 'Data gagal ditambahkan');
        }
    }

    public function destroy(Request $request, $id) {
        // $task = \App\Task::find($id);
        // $status = $task->delete();
        $status = \DB::table('task')->where('id', $id)->delete();        
        if ($status) {
            return redirect('/task')->with('success', 'Data berhsil dihapus');
        } else {
            return redirect('/task/create')->with('error', 'Data gagal dihapus');
        }
    }
    
}
