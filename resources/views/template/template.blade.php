<!doctype html>
<html lang="en">

<head>
    <link rel="icon" href="{{asset('/img/ic_logo_heroes.svg')}}" type="image/x-icon">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes|Muli&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
    <!-- timepicker -->
    <link rel="stylesheet" href="{{ asset('../plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
    <link href="{{ asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
    <!-- Date picker -->
    <link rel="stylesheet" href="{{ asset('../plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
    <link rel="stylesheet" href="{{ asset('../plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
    <!-- Date picker plugin -->
    <link rel="stylesheet" href="{{ asset('../plugins/bootstrap4-datetimepicker/bootstrap-datetimepicker.min.css')}}">
    <!-- Clock picker plugin -->
    <link rel="stylesheet" href="{{ asset('../plugins/clockpicker/css/bootstrap-clockpicker.min.css')}}">
    <!-- Date picker plugin -->
    <link rel="stylesheet" href="{{ asset('../plugins/bootstrap4-datetimepicker/bootstrap-datetimepicker.min.css')}}">    
    {{-- fullcalendar --}}
    <link rel="stylesheet" href="{{asset('../vendor/fullcalendar/css/fullcalendar.min.css')}}">
    <!-- Custom style -->
    <link rel="stylesheet" href="{{ asset('../css/index.css')}}">
          
    
    <title>Heroes schedule</title>
    <script>
        $(document).ready(function () {
            // $('.navbar-nav').on('click', 'li', function () {
                //$('.navbar-nav li.active').removeClass('active');
                // $(this).addClass('active');
                // var pathname = window.location.pathname;
	            // $('.navbar-nav > li > a[href="'+pathname+'"]').parent().addClass('active');
            // })        
        $('[data-toggle="tooltip"]').tooltip();
        });
            jQuery(window).on("load", function () {
            $('#preloader').fadeOut(500);
            $('#main-wrapper').addClass('show');            

        });

    </script>
</head>

<body>
    
    {{-- <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div> --}}        

    @yield('content')

    {{-- <script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>        

        <script src="{{ asset ('../plugins/common/common.min.js') }}"></script>
        <!-- <script src="js/custom.min.js"></script> -->

        
        <!-- timepicker -->
        <script src="{{ asset ('../plugins/moment/moment.js')}}"></script>
        <script src="{{ asset ('../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
        <!-- datepicker -->
        <script src="{{ asset('../plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
        <script src="{{ asset('../plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{ asset('../plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
        
        <script src="{{ asset('../js/plugins-init/form-pickers-init.js')}}"></script>
        <!-- Page level plugins -->
        <script src="{{ asset('../vendor/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{ asset('../vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>    
        <!-- Clock plugin -->
        <script src="{{ asset('../plugins/clockpicker/js/bootstrap-clockpicker.min.js')}}"></script>
        <script src="{{ asset('../plugins/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>        
        <!-- Date picker plugin -->
        <script src="{{ asset('../plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>    
        {{-- fullcalendar --}}
        <script src="{{ asset('../plugins/jqueryui/js/jquery-ui.min.js')}}"></script>
        <script src="{{ asset('../vendor/fullcalendar/js/fullcalendar.min.js')}}"></script>
        <script src="{{ asset('../js/plugins-init/fullcalendar-init.js')}}"></script>
        
        <!-- Page level plugins -->
        <script src="{{ asset ('../vendor/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{ asset ('../vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>        
    
    <!-- Date range Plugin JavaScript -->
    <script src="{{ asset('../plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>    
    <script src="{{ asset('../plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>    
    
    {{-- </script> --}}

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>

</html>