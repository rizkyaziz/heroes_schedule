    @extends('template.template')

    @section('content')    

    @if(session('error'))
    <div class="container-fluid">
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    </div>
    @endif

    @if(count($errors) > 0)
    <div class="container-fluid">
        <div class="alert alert-danger">
            <strong>Perhatian</strong> <br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-12 col-md-1">
                <div class="card o-hidden border-0 shadow-lg">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-12 mt-5">
                                <div class="text-center">
                                     <h1 class="h4 text-gray-900 mb-4">Data tugas</h1>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="px-5 py-4">
                                    <form action="{{ url('/task/'. @$task->id) }}" method="POST" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-sm-6"> 
                                                @csrf 

                                                @if(!empty($task))                                                    
                                                    <input type="hidden" name="_method" value="PUT">
                                                @endif
                                                
                                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                <div class="form-group">
                                                    <label for="">Name</label>
                                                    <input type="text" class="form-control" name="name" id="" aria-describedby="helpId" required
                                                        placeholder="What lesson is..." value="{{ old('task', @$task->name) }}">
                                                </div>                                                
                                                <div class="form-group">
                                                    <label for="">Media</label>
                                                    <input type="file" class="form-control" name="media" id="" aria-describedby="helpId"
                                                    placeholder="Who your teacher..." value="{{ old('media', @$task->media) }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">                                                
                                                <div class="form-group">
                                                    <label for="selectType" class="col-form-label">Type</label>
                                                    <select class="form-control" name="type" id="selectType">                                                        
                                                        <option {{@$task->type == 'Kuliah' ? 'selected' : ''}} value="Kuliah">Kuliah</option>
                                                        <option {{@$task->type == 'Kerja' ? 'selected' : ''}} value="Kerja">Kerja</option>
                                                    </select>                                           
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Deadline</label>
                                                    <input type="date" class="form-control" name="deadline" id="" aria-describedby="helpId"
                                                            placeholder="" value="{{ old('deadline', @$task->deadline) }}">
                                                </div>            
                                            </div>
                                            <div class="col-sm-12">                                                        
                                                <label for="inputfile" class="col-form-label">Description</label>
                                                <div class="form-group">
                                                    <textarea name="description" id="comment" cols="76" rows="2" class="form-control"
                                                        placeholder="Task description" value="{{ old('description', @$task->description) }}">{{ old('description', @$task->description) }}</textarea>
                                                    </div>                                                
                                                </div>
                                            </div>  
                                        </div>
                                        <div class="row mt-2 mb-5 mr-5 float-right">                                       
                                            <a href="{{ url('task')}}" class="nav-link px-5">Back</a>
                                            <button type="submit" class="btn btn-success px-5" id="next">Save</button>
                                        </div>                                        
                                    </form>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>   

    @endsection