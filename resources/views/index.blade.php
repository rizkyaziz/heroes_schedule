@extends('template.template')
@section('content')
    <div id="main-wrapper">        

         <!-- Navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-light shadow-sm fixed-top bg-white">
            <div class="container-fluid">
                <a class="navbar-brand ml-5" href="{{url('index')}}">
                    <img src="{{asset('img/ic_logo_heroes_schedule_rev.svg')}}" alt="">
                </a>
                <h4 class="mt-1 text-bold text-nav">Heroes Schedule</h4>
                <button class="navbar-toggler d-lg-none border-0" type="button" data-toggle="collapse"
                    data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <i class="fa fa-align-right" aria-hidden="true" style="color: #37AA00;"></i>
                </button>
                <!-- Collapsible content -->
                <div class="collapse navbar-collapse my-menu" id="collapsibleNavId">
                    <!-- Links -->
                    <ul class="navbar-nav ml-auto mt-2 hover main-nav">
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title active" style="font-weight: bold; border-bottom: 4px solid #37AA00;" href="{{ url('/index')}}">Beranda <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title" href="{{ url('/jadwal')}}">Jadwal </a>
                        </li>                        
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title" href="{{ url('/task')}}">Tugas </a>
                        </li>                        
                        {{-- <li class="nav-item ml-5">
                            <a class="nav-link" href="{{ url('/calendar')}}">Calendar</a>
                        </li> --}}

                        <li class="nav-item ml-5 mt-n2 dropdown">
                            <a class="nav-link nav-title" href="#" id="navbarDropdownMenuLink" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{asset('img/default-avatar.jpg')}}" width="40" height="40" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu nav-list" aria-labelledby="navbarDropdownMenuLink">                                
                                {{-- <a class="dropdown-item" href="{{ url('/profile')}}">Profile</a>   --}}
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Keluar') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </div>
                        </li>

                    </ul>
                    <!-- Links -->
                </div>
            </div>
        </nav>
        <!-- End Navigation bar -->
        
        <!-- card -->
        <div class="container-fluid mx-auto">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <!-- Card Info Mata Kuliah -->
                        @foreach ($filter as $item)
                        @if (\Carbon\Carbon::now()->translatedFormat('l') === 'Sabtu' || \Carbon\Carbon::now()->translatedFormat('l') === 'Minggu')
                            <div class="col-xl-12 col-md-12">
                                <div class="card bg-white shadow-sm h-100 p-3">
                                  <div class="card-body">
                                    <h4 class="card-title">Hari ini libur nich !</h4>
                                  </div>
                                </div>
                            </div>
                        @else
                            <div class="col-xl-6 col-md-6 mb-4">
                            <div class="card shadow-sm h-100">
                                <div class="card-body">
                                    <div class="row align-items-center no-gutters">
                                        <div class="col ml-2">
                                            <div class="text-xs font-weight-bold mb-0">
            
                                                <div class="row">
                                                    <img class="mx-2 mt-n1" src="img/SVG/ic_kuliah_green.svg" alt="">
                                                    <small class="text-uppercase font-weight-bold">{{$item->mata_kuliah}}</small>
                                                </div>
                                                <div class="row align-items-center no-gutters ml-4">
                                                    <div class="col-auto">
                                                        <h6 class="text-black-50">{{$item->ruangan}}</h6>
                                                    </div>
                                                </div>
                                                <div class="row mx-auto mb-0 mt-2">
                                                    <div class="col mx-auto text-right">
                                                    <h6 class="font-weight-bold">{{$item->jam_masuk}} - {{ $item->jam_selesai }}</h6>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        @endif
                                     
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <!-- card task kuliah -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card shadow-sm">
                                <div class="card-body">
                                    <div class="row align-items-center no-gutters">
                                        <div class="col-sm-12 mx-auto">
                                            <div class="text-xs mt-n1 col-sm-12 txt-center">
                                                <small class="text-uppercase ml-3 font-weight-bold">Tugas Kuliah</small>
                                            </div>
                                            <div class="row align-items-center nogutters mt-2 mx-auto text-number">
                                                <div class="col-sm-4 mx-auto">
                                                    <div class="h1 mb-0 mr-0 mx-auto font-weight-bold text-gray-800 text-number">{{ $kuliah->count() }}</div>
                                                </div>                                        
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Card task kerja -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card shadow-sm">
                                <div class="card-body">
                                    <div class="row align-items-center no-gutters">
                                        <div class="col mr-2">
                                            <div class="text-xs mt-n1 col-xl-12 txt-center">
                                            <small class="text-uppercase ml-3 font-weight-bold">Tugas Kerja</small>
                                            </div>
                                            <div class="row align-items-center mx-auto nogutters mt-2 text-number">
                                                <div class="col-sm-4 mx-auto">
                                                    <div class="h1 mb-0 mr-0 font-weight-bold mx-auto text-gray-800 text-number">{{$kerja->count()}}</div>
                                                </div>                                        
                                            </div>
                                        </div>                                
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Card total tugas -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card shadow-sm">
                                <div class="card-body">                                    
                                    <div class="row align-items-center no-gutters">
                                        <div class="col mr-2">                                    
                                                
                                            <div class="text-xs mt-n1 col-xl-12 txt-center">
                                            <small class="text-uppercase ml-3 font-weight-bold">Total Tugas</small>
                                            </div>
                                            <div class="row align-items-center mx-auto nogutters mt-2 text-number">
                                                <div class="col-sm-4 mx-auto">
                                                    <div class="h1 mb-0 mr-0 mx-auto font-weight-bold text-gray-800 text-number">{{$task->count()}}</div>
                                                </div>                                        
                                            </div>
                                        </div>                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <!-- table mata kuliah -->
        <div class="container-fluid mt-1">
            <div class="row">
                <!-- data tables -->
                <div class="col-xl-8 col-lg-7">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card h-100">
                                <div class="card-body">
                                    <!-- <h4 class="card-title font-weight-bold mb-4" style="font-size: 18px;">Jadwal Kuliah</h4> -->
                                    <div class="media border-bottom pb-3">
                                        <!-- <img width="35" src="ic_kuliah_green.svg" class="mr-3 rounded-circle mt-n2"> -->
                                        <div class="media-body">
                                            <h6 class="font-weight-bold" style="font-size: 18px;">Jadwal Kuliah</h6>
                                        </div>
                                        <span><a href="{{ url('jadwal')}}" class="text-black-50">Kelola <i
                                                    class="fa fa-angle-right" aria-hidden="true"></i></a></span>                                        
                                        
                                    </div>
                                    <div class="activity">
                                        <div
                                            class="table-responsive my-custom-scrollbar-y table-wrapper-scroll-y">
                                            <table class="table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>Hari</th>
                                                        <th>Jam</th>
                                                        <th>Mata Kuliah</th>
                                                        <th>SKS</th>
                                                        <th>Dosen</th>
                                                        <th>Ruang</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($jadwal_kuliah as $row)
                                                    @if ($row->hari === 'Sabtu' || $row->hari === 'Minggu' && \Carbon\Carbon::now()->translatedFormat('l'))
                                                    <tr class="table-actived">
                                                        <td class="d-none ">{{ $row->hari}}</td>
                                                        <td class="d-none ">{{ $row->jam_masuk }} - {{ $row->jam_selesai}}</td>
                                                        <td class="d-none ">{{ $row->mata_kuliah }}</td>
                                                        <td class="d-none ">{{ $row->sks }}</td>
                                                        <td class="d-none ">{{ $row->dosen }}</td>
                                                        <td class="d-none ">{{ $row->ruangan }}</td>
                                                    </tr>
                                                    @else
                                                    @if ($row->hari === \Carbon\Carbon::now()->translatedFormat('l'))
                                                        <tr class="table-actived">
                                                            <td>{{ $row->hari}}</td>
                                                            <td>{{ $row->jam_masuk }} - {{ $row->jam_selesai}}</td>
                                                            <td>{{ $row->mata_kuliah }}</td>
                                                            <td>{{ $row->sks }}</td>
                                                            <td>{{ $row->dosen }}</td>
                                                            <td>{{ $row->ruangan }}</td>                                                        
                                                        </tr>
                                                    @else                                                        
                                                        <tr>
                                                            <td>{{ $row->hari}}</td>
                                                            <td>{{ $row->jam_masuk }} - {{ $row->jam_selesai}}</td>
                                                            <td>{{ $row->mata_kuliah }}</td>
                                                            <td>{{ $row->sks }}</td>
                                                            <td>{{ $row->dosen }}</td>
                                                            <td>{{ $row->ruangan }}</td>                                                        
                                                        </tr>
                                                    @endif
                                                    @endif
                                                    @endforeach                                      
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Data Task -->
                <div class="col-xl-4 col-lg-7">
                    <div class="card" style="height: 73vh;">
                        <div class="card-body">
                            <!-- <h4 class="card-title font-weight-bold mb-4" style="font-size: 18px;">Task</h4> -->
                            <div class="media pb-3">
                                <!-- <img width="35" src="ic_kuliah_green.svg" class="mr-3 rounded-circle mt-n2"> -->
                                <div class="media-body">
                                    <h6 class="font-weight-bold" style="font-size: 18px;">Tugas</h6>
                                </div>
                                <span><a href="{{ url('task')}}" class="text-black-50">Lihat semua <i class="fa fa-angle-right"
                                            aria-hidden="true"></i></a></span>
                            </div>
                            <div class="my-custom-scrollbar-y table-wrapper-scroll-y">
                                <div id="activity">
                                    @foreach ($task as $item)                                    
                                        <div class=" pt-3 pb-3" >
                                            <div class="row align-items-start">
                                                <div class="col-1 col-md-2">
                                                    @if ($item->type === 'Kuliah' || $item->type === 'kuliah')
                                                        <img width="35" src="img/SVG/ic_kuliah_green.svg" class="mr-3 rounded-circle mt-n2">
                                                    @else
                                                        <img width="35" src="img/SVG/ic_work_green.svg" class="mr-3 rounded-circle mt-n2">
                                                    @endif
                                                </div>
                                                <div class="col-6 col-sm-5 px-0 name-task">
                                                    <h6 class="font-weight-bold mr-auto">{{$item->name}}</h6>
                                                </div>
                                                <div class="col-6 col-md-4 px-0 text-right text-deadline">
                                                    <small class="text-danger text-right">{{$item->deadline}}</small>
                                                </div>
                                            </div>
                                            <div class="row align-items-center text-deskripsi">
                                                <div class="col-8 ml-xl-5">
                                                    <p class="mb-0 text-black-50 py-2">{{$item->description}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Table mata kuliah -->

        <!-- table Picket -->
        <div class="container-fluid mt-3">
            <div class="row">
                <!-- Data piket -->
                <div class="col-xl-12 col-lg-7">
                    <div class="card pl-3">
                        <div class="card-body">
                            <div class="media mt-2 pb-4">
                                <img width="35" src="img/SVG/ic_picket.svg" class="mr-3 rounded-circle mt-n2">
                                <div class="media-body">
                                    <h6 class="font-weight-bold" style="font-size: 18px;">Piket hari ini</h6>
                                </div>
                                <span class="text-muted{"> {{ Carbon\Carbon::now()->translatedFormat('l d F Y')}} </span>
                            </div>                            
                            <div id="activity mt-5 pl-5">
                                <table class="table">                                    
                                    <tbody>
                                        @foreach ($filter_piket as $item)
                                        @if (\Carbon\Carbon::now()->translatedFormat('l') === 'Sabtu' || \Carbon\Carbon::now()->translatedFormat('l') === 'Minggu')
                                            <tr>
                                                <td>Kosong</td>
                                                <td>Kosong</td>
                                            </tr>
                                        @else
                                        
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="row mt-4 mb-3">
                                    @foreach ($filter_hari as $item)
                                        
                                    @endforeach
                                    
                                    <div class="col-sm-7">
                                        @foreach ($filter_piket as $item)
                                            <div class="row p-1 mb-2 card-piket-responsive">
                                                    <div class="col-sm-5 pr-0 piket">
                                                        <div class="card-piket card-piket-border-right text-center w-100">
                                                            <div class="card-text">{{$item->nama}}</div>
                                                        </div>
                                                    </div>                        
                                                    <div class="separator mt-4 m-0"></div>
                                                    @if (empty($item->tugas))
                                                        <div class="col-sm-5 pl-0 piket">
                                                            <div class="card-piket card-piket-empty text-center w-100">
                                                                <div class="card-text ">Tidak ada tugas</div>
                                                            </div>
                                                        </div>
                                                    @else                                                        
                                                        <div class="col-sm-5 pl-0 piket">
                                                            <div class="card-piket text-center  w-100">
                                                                <div class="card-text ">{{$item->tugas}}</div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>                                            
                                            @endforeach
                                        </div>
                                        <div class="col-sm-5 text-desc">
                                            <div class="media pb-3">
                                            <div class="media-body text-black-50">
                                                <p><span class="text-danger h5">*</span> Menyapu dan mengepel lantai dilakukan setiap pagi.</p>
                                                <p><span class="text-danger h5">*</span> Jika hoream menyapu dan mengepel boleh memesan Go Clean.</p>
                                                <p><span class="text-danger h5">*</span> Menanak nasi pagi sebanyak 7 cangkir dan malam sebanyak 4 <span class="ml-3"> cangkir,  jika full team dikondisikan saja.</span></p>
                                            </div>
                                        </div>
                                        </div>
                                    </div>                                    
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <!-- End Table piket -->

        <!-- Modal -->
        <div class="modal fade" id="ModalFormNewTask" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h5 class="modal-title text-white">New Task</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body px-4">
                        <div class="container">
                            <div class="row">
                                <h6 class="py-3">General Details</h6>
                            </div>
                            <div class="row m-2">
                                <form>
                                    <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="staticEmail"
                                                placeholder="Task Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="selectType" class="col-sm-3 col-form-label">Tipe</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="" id="selectType">
                                                <option>Pilih tipe tugas</option>
                                                <option>Kuliah</option>
                                                <option>Kerja</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputfile" class="col-sm-3 col-form-label">Media</label>
                                        <div class="col-sm-8">
                                            <input type="file" class="form-control" id="inputfile"
                                                placeholder="Choose file">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputfile" class="col-sm-3 col-form-label">Description</label>
                                        <div class="col-sm-8">
                                            <textarea name="" id="" cols="33" rows="3"
                                                placeholder="Task description"></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="row">
                                <h6 class="p-y3">Timeline</h6>
                            </div>
                            <div class="row ml-4">
                                <form>
                                    <div class="form-group row">
                                        <label for="date" class=" col-form-label">Due date</label>
                                        <div class="col ml-5 input-group">
                                            <input type="date" class="form-control" id="date" placeholder="dd/MM/yyyy">
                                            <div class="input-group-append">
                                                <span class="fa fa-calendar input-group-text" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Your Email">
                                    <div class="input-group-append">
                                        <span class="input-group-text">@example.com</span>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal lanjutan -->
        <div class="modal fade" id="ModalFormCenterLanjut" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content border-0">
                    <div class="modal-header bg-success">
                        <h5 class="modal-title text-white">Tambah Jadwal</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body px-4">
                        <div class="container">
                            <form action="">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb3 mb-sm-0">
                                        <label for="">Name</label>
                                        <!-- <div class="input-group clockpicker"> -->
                                        <input type="text" class="form-control" placeholder="Task name...">
                                        <!-- </div> -->
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="">Type</label>
                                        <select class="form-control" name="" id="selectType">
                                            <option>Select the type of task</option>
                                            <option>Kuliah</option>
                                            <option>Kerja</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Media</label>
                                    <input type="file" name="" id="" class="form-control form-control-user">
                                </div>
                                <div class="form-group">
                                    <label for="">Description</label> <br>
                                    <textarea name="" id="" cols="50" rows="2"
                                        placeholder="Task description..."></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="m-t-40">Due Date</label>
                                    <div class="input-group clockpicker" data-placement="bottom" data-align="top"
                                        data-autoclose="true">
                                        <!-- <label class="m-t-40">Default Material Date Timepicker</label> -->
                                        <input type="text" id="date-format" class="form-control"
                                            placeholder="Saturday 24 June 2017 - 21:44">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer mb-3 mx-4">
                        <button type="button" class="btn btn-success btn-block">Finish</button>
                    </div>
                </div>
            </div>
        </div>     

        
    </div>    
@endsection
