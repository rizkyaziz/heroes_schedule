@extends('template.template')

@section('content')
     <!-- Navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-light shadow-sm fixed-top bg-white">
            <div class="container-fluid">
                <a class="navbar-brand ml-5" href="{{url('index')}}">
                    <img src="{{asset('img/ic_logo_heroes_schedule_rev.svg')}}" alt="">
                </a>
                <h4 class="mt-1 text-nav">Heroes Schedule</h4>
                <button class="navbar-toggler d-lg-none border-0" type="button" data-toggle="collapse"
                    data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <i class="fa fa-align-right" aria-hidden="true" style="color: #37AA00;"></i>
                </button>
                <!-- Collapsible content -->
                <div class="collapse navbar-collapse my-menu" id="collapsibleNavId">
                    <!-- Links -->
                    <ul class="navbar-nav ml-auto mt-2 hover main-nav">
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title" href="{{ url('/index')}}">Beranda <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title"  href="{{ url('/jadwal')}}">Jadwal </a>
                        </li>                        
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title active" style="font-weight: bold; border-bottom: 4px solid #37AA00;"  href="{{ url('/task')}}">Tugas </a>
                        </li>                        
                        {{-- <li class="nav-item ml-5">
                            <a class="nav-link" href="{{ url('/calendar')}}">Calendar</a>
                        </li> --}}

                        <li class="nav-item ml-5 mt-n2 dropdown">
                            <a class="nav-link nav-title" href="#" id="navbarDropdownMenuLink" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{asset('img/default-avatar.jpg')}}" width="40" height="40" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu nav-list" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Keluar') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>

                    </ul>
                    <!-- Links -->
                </div>
            </div>
        </nav>
<!-- End Navigation bar -->   

    <!-- Breadcrumb -->
    <div class="container-fluid">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{url('index')}}"><i class="fa fa-Beranda" aria-hidden="true"></i> Beranda</a></li>
            <!-- <li class="breadcrumb-item"><a href="#"></a></li> -->
            <li class="breadcrumb-item active"><i class="fa fa-check" aria-hidden="true"></i> Tugas</li>
        </ol>
    </div>
    <!-- End Breadcrumb -->
    
    @if(session('success'))
        <div class="container-fluid">
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        </div>
    @endif

    @if(session('error'))
        <div class="container-fluid">
            <div class="alert alert-error">
                {{ session('error') }}
            </div>
        </div>
    @endif

    <div class="container-fluid">
        <div class="row p-3">
            <div class="col-sm-6">
                <div class="card shadow">
                     <div class="card-header" style="background-color: rgba(131, 125, 250, 0.171);">
                        <div class="media">                            
                            <div class="media-body">
                                <h5 class="card-title" style="color: #847DFA;">Kuliah <span style="background-color: #847DFA;" class="badge badge-success px-2">{{ $kuliah->count() }}</span></h5>
                            </div>                            
                            <button type="button" class="btn bg-transparent" data-dismiss=""
                                data-toggle="modal" data-toggle="tooltip"
                                title="Add Data">
                                <a href="{{ url('/task/create') }}"><i class="fa fa-plus" style="color: #847DFA;" aria-hidden="true"></i></a>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach ($kuliah as $item)
                                <div class="col-sm-6">
                                    <div class="card m-2 w-100" style="border: 1px solid #847DFA">
                                        <div class="card-body">
                                            <div class="media pb-3">
                                                <div class="media-body">
                                                    <h6 class="card-title" for="modal-input-name" id="modal-input-name">{{$item->name}}</h6>
                                                </div>
                                                <p class="card-text text-danger text-small">{{$item->deadline}}</p>
                                            </div>
                                            <p class="card-text text-muted">{{$item->description}}</p>
                                        </div>
                                        @if (!empty($item->media))   
                                                <a href="{{url('/storage/task', $item->media)}}">
                                                <img class="card-img-bottom p-2 img-responsive" src="{{ url('/storage/task', $item->media) }}" alt="Card image cap"s>
                                                </a>                                    
                                        @else
                                        <input type="hidden">
                                        @endif                                       
                                        <div class="btn-group ml-auto">
                                            <button type="button" class="btn text-warning">
                                                <a href="{{ url('/task/'. $item->id . '/edit') }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            </button>                                            
                                            <form action="{{ url('/task', $item->id) }}" method="POST" class="btn">
                                                @method('DELETE')
                                                @csrf                                                                                            
                                                <button type="submit" class="btn "><i class="fa fa-trash text-danger" aria-hidden="true"></i></button>
                                            </form>                                            
                                        </div>
                                    </div> 
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card shadow">
                    <div class="card-header" style="background-color: rgba(242, 255, 67, 0.192);">
                        <div class="media">                            
                            <div class="media-body">
                                <h5 class="card-title text-warning">Kerja <span class="bg-warning badge badge-success px-2">{{ $kerja->count() }}</span></h5>
                            </div>                            
                            <button type="button" class="btn bg-transparent" data-dismiss=""
                                data-toggle="modal" data-toggle="tooltip"
                                title="Add Data">
                                <a href="{{ url('/task/create') }}"><i class="fa fa-plus text-warning" aria-hidden="true"></i></a>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach ($kerja as $item)
                                <div class="col-sm-6">
                                    <div class="card m-2 w-100" style="border: 1px solid #ffc107">
                                        <div class="card-body">
                                            <div class="media pb-3">
                                                <div class="media-body">
                                                    <h6 class="card-title" for="modal-input-name" id="modal-input-name">{{$item->name}}</h6>
                                                </div>
                                                <p class="card-text text-danger text-small">{{$item->deadline}}</p>
                                            </div>
                                            <p class="card-text text-muted">{{$item->description}}</p>
                                        </div>
                                        @if (!empty($item->media))   
                                                <a href="{{url('/storage/task', $item->media)}}">
                                                <img class="card-img-bottom p-2 img-responsive" src="{{ url('/storage/task', $item->media) }}" alt="Card image cap"s>
                                                </a>                                    
                                        @else
                                        <input type="hidden">
                                        @endif                                    
                                        <div class=" btn-group ml-auto">
                                            <button type="button" class="btn text-warning">
                                                <a href="{{ url('/task/'. $item->id . '/edit') }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            </button>                                            
                                            <form action="{{ url('/task', $item->id) }}" method="POST" class="btn">
                                                @method('DELETE')
                                                @csrf                                                                                            
                                                <button type="submit" class="btn "><i class="fa fa-trash text-danger" aria-hidden="true"></i></button>
                                            </form>                                            
                                        </div>
                                    </div> 
                                </div>
                            @endforeach
                        </div>                               
                    </div>                    
                </div>                                
            </div>
        </div>
    </div>    

    <!-- Modal -->
    <div class="modal fade" id="ModalCenterInfo" tabindex="-1" role="dialog" aria-labelledby="ModalCenterInfo"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content px-4">
                <div class="modal-header mt-n1">
                    <!-- <h5 class="modal-title">Pemrograman Web</h5> -->
                    <div class="row m-2">
                        <h5 class="modal-title">Pemrograman Web</h5>
                    </div>
                    <button type="button" class="close mr-n4" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mt-5">
                    <div class="progress" style="height: 8px; background-color: #ABDB94;">
                        <div class="progress-bar" style="width: 70%; background-color: #37AA00;"></div>
                    </div>
                    <div class="row py-3">
                        <div class="col-auto">
                            <img src="ic_assign_line.svg" alt="" width="50px">
                        </div>
                        <div class="col mt-1">
                            <h6 class="font-weight-bold">Pemrograman Web</h6>
                            <div class="text-xs text-danger"><i class="fa fa-clock-o text-success"
                                    aria-hidden="true"></i> Due Thursday, October 20</div>
                        </div>
                    </div>

                    <div class="row mt-5 mx-1">
                        <h6>Description</h6>
                        <div class="text-black-50">Membangun website e-commerce menggunakan framwork laravel</div>
                    </div>
                </div>
                <div class="modal-footer mb-2 mt-5">
                    <button type="button" class="btn btn-warning rounded-circle mr-3" data-dismiss=""><i
                            class="fa fa-pencil text-white" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-danger rounded-circle"><i class="fa fa-trash"
                            aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
    </div>    

    

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

@endsection