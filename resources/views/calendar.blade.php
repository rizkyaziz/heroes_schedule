
@extends('template.template')
    
    @section('content')
     <!-- Navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-light shadow-sm fixed-top bg-white">
            <div class="container-fluid">
                <a class="navbar-brand ml-5" href="{{url('index')}}">
                    <img src="{{asset('img/ic_logo_heroes_schedule_rev.svg')}}" alt="">
                </a>
                <button class="navbar-toggler d-lg-none border-0" type="button" data-toggle="collapse"
                    data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <i class="fa fa-align-right" aria-hidden="true" style="color: #37AA00;"></i>
                </button>
                <!-- Collapsible content -->
                <div class="collapse navbar-collapse my-menu" id="collapsibleNavId">
                    <!-- Links -->
                    <ul class="navbar-nav ml-auto mt-2 hover main-nav">
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title" href="{{ url('/index')}}">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title" href="{{ url('/jadwal')}}">Jadwal </a>
                        </li>                        
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title" href="{{ url('/task')}}">Task </a>
                        </li>                        
                        <li class="nav-item ml-5">
                            <a class="nav-link active" style="font-weight: bold; border-bottom: 4px solid #37AA00;"  href="{{ url('/calendar')}}">Calendar</a>
                        </li>

                        <li class="nav-item ml-5 mt-n2 dropdown">
                            <a class="nav-link nav-title" href="#" id="navbarDropdownMenuLink" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{asset('img/default-avatar.jpg')}}" width="40" height="40" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu nav-list" aria-labelledby="navbarDropdownMenuLink">                                
                                {{-- <a class="dropdown-item" href="login.html">Log Out</a> --}}
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </div>
                        </li>

                    </ul>
                    <!-- Links -->
                </div>
            </div>
        </nav>
<!-- End Navigation bar -->

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>

<body>

 

  <div class="container" style="margin-top:10%">

      <div class="response"></div>

      <div id='calendar'></div>  

  </div>

        
        <script>

        $(document).ready(function () {

                

                var SITEURL = "{{url('/')}}";

                $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

                });

        

                var calendar = $('#calendar').fullCalendar({

                    editable: true,

                    events: SITEURL + "/fullcalendareventmaster",

                    displayEventTime: true,

                    editable: true,

                    eventRender: function (event, element, view) {

                        if (event.allDay === 'true') {

                            event.allDay = true;

                        } else {

                            event.allDay = false;

                        }

                    },

                    selectable: true,

                    selectHelper: true,

                    select: function (start, end, allDay) {

                        var title = prompt('Event Title:');

        

                        if (title) {

                            var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");

                            var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");

        

                            $.ajax({

                                url: SITEURL + "/fullcalendareventmaster/create",

                                data: 'title=' + title + '&start=' + start + '&end=' + end,

                                type: "POST",

                                success: function (data) {

                                    displayMessage("Added Successfully");

                                }

                            });

                            calendar.fullCalendar('renderEvent',

                                    {

                                        title: title,

                                        start: start,

                                        end: end,

                                        allDay: allDay

                                    },

                            true

                                    );

                        }

                        calendar.fullCalendar('unselect');

                    },

                    

                    eventDrop: function (event, delta) {

                                var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");

                                var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");

                                $.ajax({

                                    url: SITEURL + '/fullcalendareventmaster/update',

                                    data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,

                                    type: "POST",

                                    success: function (response) {

                                        displayMessage("Updated Successfully");

                                    }

                                });

                            },

                    eventClick: function (event) {

                        var deleteMsg = confirm("Do you really want to delete?");

                        if (deleteMsg) {

                            $.ajax({

                                type: "POST",

                                url: SITEURL + '/fullcalendareventmaster/delete',

                                data: "&id=" + event.id,

                                success: function (response) {

                                    if(parseInt(response) > 0) {

                                        $('#calendar').fullCalendar('removeEvents', event.id);

                                        displayMessage("Deleted Successfully");

                                    }

                                }

                            });

                        }

                    }

        

                });

        });

        

        function displayMessage(message) {

            $(".response").html("
        "+message+"
        ");

            setInterval(function() { $(".success").fadeOut(); }, 1000);

        }

        </script>
    @endsection
    <div class="myCalendar"></div>
    
    <script>
    $('.myCalendar').calendar({
      date: new Date(),
      autoSelect: false, // false by default
      select: function (date) {
        console.log('SELECT', date)
      },
      toggle: function (y, m) {
        console.log('TOGGLE', y, m)
      }
    })
  </script>