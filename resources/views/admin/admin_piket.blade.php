@extends('template.template')
@section('content')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --}}
<!-- Navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-light shadow-sm fixed-top bg-white">
            <div class="container-fluid">
                <a class="navbar-brand ml-5" href="{{url('index')}}">
                    <img src="{{asset('img/ic_logo_heroes_schedule_rev.svg')}}" alt="">
                </a>
                <button class="navbar-toggler d-lg-none border-0" type="button" data-toggle="collapse"
                    data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <i class="fa fa-align-right" aria-hidden="true" style="color: #37AA00;"></i>
                </button>
                <!-- Collapsible content -->
                <div class="collapse navbar-collapse my-menu" id="collapsibleNavId">
                    <h4 class="mt-2" >Selamat datang admin piket</h4>
                    <!-- Links -->
                    <ul class="navbar-nav ml-auto mt-2 hover main-nav">                                                

                        <li class="nav-item ml-5 mt-n2 dropdown">
                            <a class="nav-link nav-title" href="#" id="navbarDropdownMenuLink" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{asset('img/default-avatar.jpg')}}" width="40" height="40" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu nav-list" aria-labelledby="navbarDropdownMenuLink">                                
                                {{-- <a class="dropdown-item" href="login.html">Log Out</a> --}}
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                    <!-- Links -->
                </div>
            </div>
        </nav>
<!-- End Navigation bar -->

<div class="main-wrapper">    
    <script>
        $(document).ready(function(){            
            $(".hide").hide();
            $(".hide-tue").hide();
            $(".hiden").hide();
            $(".hide-wed").hide();
            $(".hide-thu").hide();
            $(".hide-fri").hide();

            $(".show").click(function(){
                $(".hiden").slideToggle();
            });
            $(".show-mon").click(function(){
                $(".hide").slideToggle();
            });
            $(".show-tue").click(function(){
                $(".hide-tue").slideToggle();
            });
            $(".show-wed").click(function(){
                $(".hide-wed").slideToggle();
            });
            $(".show-thu").click(function(){
                $(".hide-thu").slideToggle();
            });
            $(".show-fri").click(function(){
                $(".hide-fri").slideToggle();
            });            
        });
    </script>

    <!-- table piket -->
    <div class="container-fluid mt-3 mb-5">
        <div class="row">
            <!-- data tables -->
            <div class="col-xl-12 col-lg-7">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card h-100">
                            <div class="card-body p-4">
                                <div class="media pt-2 pb-4" data-target="#ModalCenterInfo" data-toggle="modal">
                                    <img width="35" src="img/SVG/ic_picket.svg" class="mr-3 rounded-circle mt-n2">
                                    <div class="media-body">
                                        <h6 class="font-weight-bold text-success" style="font-size: 18px;">
                                             Jadwal Piket
                                        </h6>
                                    </div>
                                    <a href="{{ url('/admin_piket/create') }}">
                                        <button type="button" class="btn btn-success mr-3" data-dismiss=""
                                            data-target="#ModalFormCenter" data-toggle="modal" data-toggle="tooltip"
                                            title="Add Data"><i class="fa fa-plus text-white"
                                            aria-hidden="true"></i>
                                        </button>
                                    </a>
                                </div>                                
                                <div class="activity">
                                    <div class="table">
                                        <div class="row">
                                            <table class="table ml-2">
                                                <thead>
                                                    <tr>
                                                        <th>Hari</th>
                                                        @foreach ($isset as $naon)
                                                        <th>Anggota {{ isset($i) ? ++ $i : $i = 1 }}</th>
                                                        @endforeach                                                                                                 
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="font-weight: bold; color:black; width: 10%;">Senin <button class=" bg-transparent border-0 ml-xl-4 show-mon" id="show"><i class="fa fa-cog text-secondary" aria-hidden="true"></i></button></td>
                                                        @foreach ($piket_senin as $row)                                                                                                            
                                                                                                                                                 
                                                        <td class="showhim">
                                                            <div class="p-2 mt-3 ml-xl-5 bg-white shadow position-absolute hide">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <a class="text-info" href="{{ url('/admin_edit/'. $row->id .'/edit') }}">
                                                                        <button class="btn btn-rounded-circle text-info"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                                    </a>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <form action="{{ url('/admin_delete', $row->id) }}" method="POST">
                                                                            @method('DELETE')
                                                                            @csrf
                                                                            <button type="submit" class="btn rounded-circle bg-transparent"><i class="fa fa-trash text-danger" aria-hidden="true"></i> </button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            {{ $row->nama }}
                                                        </td>                                                        
                                                        @endforeach
                                                        <td><button class="btn rounded-circle show"><i class="fa fa-info-circle text-info" aria-hidden="true"></i></button></td>
                                                    </tr>
                                                    <tr class="hiden" style="background-color:#d2ffd134">
                                                        <td>Tugas</td>
                                                        @foreach ($piket_senin as $row)
                                                        <td>{{$row->tugas}}</td>                                                  
                                                        @endforeach
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td style="font-weight: bold; color:black; width: 10%;">Selasa <button class=" bg-transparent border-0 ml-sm-3 show-tue"><i class="fa fa-cog text-secondary" aria-hidden="true"></i></button></td>
                                                        @foreach ($piket_selasa as $row)
                                                        <td>
                                                            <div class="p-2 mt-3 ml-xl-5 bg-white shadow position-absolute hide-tue">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <a class="text-info" href="{{ url('/admin_edit/'. $row->id .'/edit') }}">
                                                                        <button class="btn btn-rounded-circle text-info"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                                    </a>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <form action="{{ url('/admin_delete', $row->id) }}" method="POST">
                                                                            @method('DELETE')
                                                                            @csrf
                                                                            <button type="submit" class="btn rounded-circle bg-transparent"><i class="fa fa-trash text-danger" aria-hidden="true"></i> </button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            {{ $row->nama }}
                                                        </td>
                                                        @endforeach
                                                    </tr>
                                                    <tr class="hiden"  style="background-color:#d2ffd134">
                                                        <td>Tugas</td>
                                                        @foreach ($piket_selasa as $row)
                                                        <td>{{$row->tugas}}</td>                                                        
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold; color:black; width: 10%;">Rabu <button class=" bg-transparent border-0 ml-sm-4 show-wed" id="show"><i class="fa fa-cog text-secondary" aria-hidden="true"></i></button></td>
                                                        @foreach ($piket_rabu as $row)
                                                        <td>
                                                            <div class="p-2 mt-3 ml-xl-5 bg-white shadow position-absolute hide-wed">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <a class="text-info" href="{{ url('/admin_edit/'. $row->id .'/edit') }}">
                                                                        <button class="btn btn-rounded-circle text-info"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                                    </a>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <form action="{{ url('/admin_delete', $row->id) }}" method="POST">
                                                                            @method('DELETE')
                                                                            @csrf
                                                                            <button type="submit" class="btn rounded-circle bg-transparent"><i class="fa fa-trash text-danger" aria-hidden="true"></i> </button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            {{ $row->nama }}
                                                        </td>
                                                        @endforeach
                                                    </tr>
                                                    <tr class="hiden"  style="background-color:#d2ffd134">
                                                        <td>Tugas</td>
                                                        @foreach ($piket_rabu as $row)
                                                        <td>{{$row->tugas}}</td>                                                        
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold; color:black; width: 10%;">Kamis <button class=" bg-transparent border-0 ml-xl-3 show-thu"><i class="fa fa-cog text-secondary" aria-hidden="true"></i></button></td>
                                                        @foreach ($piket_kamis as $row)
                                                        <td>
                                                            <div class="p-2 mt-3 ml-xl-5 bg-white shadow position-absolute hide-thu">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <a class="text-info" href="{{ url('/admin_edit/'. $row->id .'/edit') }}">
                                                                        <button class="btn btn-rounded-circle text-info"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                                    </a>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <form action="{{ url('/admin_delete', $row->id) }}" method="POST">
                                                                            @method('DELETE')
                                                                            @csrf
                                                                            <button type="submit" class="btn rounded-circle bg-transparent"><i class="fa fa-trash text-danger" aria-hidden="true"></i> </button>
                                                                        </form>
                                                                    </div>
                                                                </div>                                                          
                                                            </div>
                                                            
                                                            {{ $row->nama }}
                                                        </td>
                                                        @endforeach
                                                    </tr>
                                                    <tr class="hiden"  style="background-color:#d2ffd134">
                                                        <td>Tugas</td>
                                                        @foreach ($piket_kamis as $row)
                                                        <td>{{$row->tugas}}</td>                                                        
                                                        @endforeach
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold; color:black; width: 10%;">Jumat <button class=" bg-transparent border-0 ml-sm-3 show-fri"><i class="fa fa-cog text-secondary" aria-hidden="true"></i></button></td>
                                                        @foreach ($piket_jumat as $row)
                                                        <td>
                                                            <div class="p-2 mt-3 ml-xl-5 bg-white shadow position-absolute hide-fri">
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <a class="text-info" href="{{ url('/admin_edit/'. $row->id .'/edit') }}">
                                                                        <button class="btn btn-rounded-circle text-info"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                                    </a>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <form action="{{ url('/admin_delete', $row->id) }}" method="POST">
                                                                            @method('DELETE')
                                                                            @csrf
                                                                            <button type="submit" class="btn rounded-circle bg-transparent"><i class="fa fa-trash text-danger" aria-hidden="true"></i> </button>
                                                                        </form>
                                                                    </div>
                                                                </div>                                                          
                                                            </div>
                                                            
                                                            {{ $row->nama }}
                                                        </td>
                                                        @endforeach
                                                    </tr>
                                                    <tr class="hiden"  style="background-color:#d2ffd134">
                                                        <td>Tugas</td>
                                                        @foreach ($piket_jumat as $row)
                                                        <td>{{$row->tugas}}</td>                                                        
                                                        @endforeach
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <!-- End Table Piket -->
</div>
@endsection