@extends('template.template')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
@section('content')
    <div class="container bg-white p-4">
        <div class="row text-center">
            @foreach ($profile as $item)
            <div class="col-sm-3">
                <div class="row">
                    <div class="col-sm-12">
                    @if (!empty($item->foto))                    
                        <img class="card-img-bottom p-2 img-responsive" src="{{ $item->foto }}" alt="Card image cap"s>
                    @else                        
                        <img id="ganti" src="/img/default-avatar.jpg" alt="" width="80" height="80" class="rounded-circle" style="cursor:pointer">
                    @endif
                </div>
            </div>
            <div class="row my-3"  id="show-hide" style="display:none">
                <div class="row">                    
                    <div class="col-sm-12 my-2">
                        <form action="{{ ('/profile'. @$profile) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @if(!empty($profile))                      
                            <input type="hidden" name="_method" value="PUT">
                            @endif
                            
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <input type="file" size="60" class="form-control">
                            <input type="submit" class="pull-right btn btn-sm btn-primary">
                        </form>                            
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-sm-4 p-3">
                <div class="row">
                    <div class="col-sm-12 my-2 border-bottom">
                        <h6>Samantha wiliam</h6>
                    </div>
                    <div class="col-sm-12 my-2 border-bottom">
                        <h6>samantha212@gmail.co</h6>
                    </div>
                    <div class="col-sm-12 my-2 border-bottom">
                        <h6>samantha212@gmail.co</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 p3">
                <h6>asldasj</h6>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#ganti').click(function() {
                $('#show-hide').toggle(500);
            });
        });
    </script>
@endsection