    @extends('template.template')

    @section('content')    

    @if(session('error'))
    <div class="container-fluid">
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    </div>
    @endif

    @if(count($errors) > 0)
    <div class="container-fluid">
        <div class="alert alert-danger">
            <strong>Perhatian</strong> <br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-12 col-md-1">
                <div class="card o-hidden border-0 shadow-lg">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-12 mt-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Tambahkan Anggota Piket</h1>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="px-5 py-4">
                                    <form action="{{ url('/admin_piket/'. @$jadwal_piket->id) }}" method="POST">
                                        
                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                @csrf

                                                @if(!empty($jadwal_piket))
                                                    <input type="hidden" name="_method" value="PUT">
                                                @endif

                                                
                                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                
                                                <div class="form-group">
                                                    <label for="">Hari</label>
                                                    <select class="form-control" name="hari" id=""
                                                        aria-placeholder="When they started">
                                                        <option {{@$jadwal_kuliah->hari == 'Senin' ? 'selected' : ''}} value="Senin">Senin</option>
                                                        <option {{@$jadwal_kuliah->hari == 'Selasa' ? 'selected' : ''}} value="Selasa">Selasa</option>
                                                        <option {{@$jadwal_kuliah->hari == 'Rabu' ? 'selected' : ''}} value="Rabu">Rabu</option>
                                                        <option {{@$jadwal_kuliah->hari == 'Kamis' ? 'selected' : ''}} value="Kamis">Kamis</option>
                                                        <option {{@$jadwal_kuliah->hari == 'Jumat' ? 'selected' : ''}} value="Jumat">Jumat</option>
                                                    </select>
                                                </div>
                                                {{-- <div class="form-group">
                                                    <label for="">Minggu</label>
                                                    <select class="form-control" name="minggu" id=""
                                                        aria-placeholder="When did you start class..." value="{{ old('minggu', @$jadwal_piket->minggu) }}">                                                        
                                                        <option >Ganjil</option>
                                                        <option>Genap</option>
                                                    </select>f
                                                </div> --}}
                                                <div class="form-group">
                                                    <label for="">Anggota</label>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" name="nama1" id="" aria-describedby="helpId"
                                                                placeholder="Masukkan nama anggota ..." value="{{ old('nama', @$jadwal_piket->nama) }}">
                                                            <input type="text" class="form-control mt-2" name="nama2" id="" aria-describedby="helpId"
                                                                placeholder="Masukkan nama anggota ..." value="{{ old('nama', @$jadwal_piket->nama) }}">
                                                            <input type="text" class="form-control mt-2" name="nama3" id="" aria-describedby="helpId"
                                                                placeholder="Masukkan nama anggota ..." value="{{ old('nama', @$jadwal_piket->nama) }}">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" name="nama4" id="" aria-describedby="helpId"
                                                                placeholder="Masukkan nama anggota ..." value="{{ old('nama', @$jadwal_piket->nama) }}">
                                                            <input type="text" class="form-control mt-2" name="nama5" id="" aria-describedby="helpId"
                                                                placeholder="Masukkan nama anggota ..." value="{{ old('nama', @$jadwal_piket->nama) }}">
                                                            <input type="text" class="form-control mt-2" name="nama6" id="" aria-describedby="helpId"
                                                                placeholder="Masukkan nama anggota ..." value="{{ old('nama', @$jadwal_piket->nama) }}">
                                                        </div>
                                                    </div>                                         
                                                </div>
                                            </div>                                           
                                        </div>
                                        <div class="row mt-3 mb-5 mr-1 float-right">                                       
                                            <a href="{{ url('admin_index')}}" class="nav-link px-5">Kembali</a>
                                            <button type="submit" class="btn btn-success px-5" id="next">Simpan</button>
                                        </div>                                        
                                    </form>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>

    @endsection