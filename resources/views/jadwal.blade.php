@extends('template.template')

@section('content')    

<!-- Navigation bar -->
    <nav class="navbar navbar-expand-lg navbar-light shadow-sm fixed-top bg-white">
            <div class="container-fluid">
                <a class="navbar-brand ml-5" href="{{url('index')}}">
                    <img src="{{asset('img/ic_logo_heroes_schedule_rev.svg')}}" alt="">
                </a>
                <h4 class="mt-1 text-nav">Heroes Schedule</h4>
                <button class="navbar-toggler d-lg-none border-0" type="button" data-toggle="collapse"
                    data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <i class="fa fa-align-right" aria-hidden="true" style="color: #37AA00;"></i>
                </button>
                <!-- Collapsible content -->
                <div class="collapse navbar-collapse my-menu" id="collapsibleNavId">
                    <!-- Links -->
                    <ul class="navbar-nav ml-auto mt-2 hover main-nav">
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title" href="{{ url('/index')}}">Beranda <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title active" style="font-weight: bold; border-bottom: 4px solid #37AA00;"  href="{{ url('/jadwal')}}">Jadwal </a>
                        </li>                        
                        <li class="nav-item ml-5">
                            <a class="nav-link nav-title" href="{{ url('/task')}}">Tugas </a>
                        </li>                        
                        {{-- <li class="nav-item ml-5">
                            <a class="nav-link" href="{{ url('/calendar')}}">Calendar</a>
                        </li> --}}

                        <li class="nav-item ml-5 mt-n2 dropdown">
                            <a class="nav-link nav-title" href="#" id="navbarDropdownMenuLink" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{asset('img/default-avatar.jpg')}}" width="40" height="40" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu nav-list" aria-labelledby="navbarDropdownMenuLink">                                
                                {{-- <a class="dropdown-item" href="login.html">Log Out</a> --}}
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Keluar') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </div>
                        </li>

                    </ul>
                    <!-- Links -->
                </div>
            </div>
    </nav>
<!-- End Navigation bar -->

<!-- Breadcrumb -->
    <div class="container-fluid mt-4">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{url('index')}}"><i class="fa fa-Beranda" aria-hidden="true"></i> Beranda</a></li>
            <!-- <li class="breadcrumb-item"><a href="#"></a></li> -->
            <li class="breadcrumb-item active"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Jadwal</li>
        </ol>
    </div>
<!-- End Breadcrumb -->
    @if (session('success'))
        <div class="alert alert-success">
          <p>{{ session('success') }}</p>
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-error">
            <p>{{ session('error') }}</p>
        </div>
    @endif
<!-- table mata kuliah -->
    <div class="container-fluid mt-3 mb-5">
        <div class="row">            
            <div class="col-xl-12 col-lg-7">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card h-100">
                            <div class="card-body">
                                <div class="media pt-2 pb-4">
                                    <img width="35" src="img/SVG/ic_kuliah_green.svg" class="mr-3 rounded-circle mt-n2">
                                    <div class="media-body">
                                        <h6 class="font-weight-bold text-success" style="font-size: 18px;">Jadwal Kuliah
                                        </h6>
                                    </div>
                                    <a href="{{ url('/jadwal/create') }}">
                                        <button type="button" class="btn btn-success rounded-circle mr-3" data-dismiss=""
                                            data-target="#ModalFormCenter">
                                            <i class="fa fa-plus text-white" ria-hidden="true"></i>
                                        </button>
                                    </a>       
                                    <!-- <span class="text-danger">Oct 24, 2019</span> -->
                                </div>
                                <!-- <h4 class="card-title font-weight-bold mb-4" style="font-size: 18px;">Jadwal Kuliah</h4> -->
                                <div class="activity">
                                    <div class="table-responsive">
                                        <table class="table table-xs m-0 table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Hari</th>
                                                    <th>Jam</th>
                                                    <th>Mata Kuliah</th>
                                                    <th>SKS</th>
                                                    <th>Dosen</th>
                                                    <th>Ruang</th>                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($jadwal_kuliah as $row)
                                                @if ($row->hari === 'Sabtu' || $row->hari === 'Minggu' && \Carbon\Carbon::now()->translatedFormat('l'))
                                                    <tr class=" showhim">
                                                        <td class="d-none">{{ $row->hari}}</td>
                                                        <td class="d-none">{{ $row->jam_masuk }} - {{ $row->jam_selesai}}</td>
                                                        <td class="d-none">{{ $row->mata_kuliah }}</td>
                                                        <td class="d-none">{{ $row->sks }}</td>
                                                        <td class="d-none">{{ $row->dosen }}</td>
                                                        <td class="d-none">{{ $row->ruangan }}</td>
                                                    </tr>
                                                @else
                                                @if ($row->hari === \Carbon\Carbon::now()->translatedFormat('l'))
                                                    <tr class="showhim table-actived">
                                                        <td>{{$row->hari}}</td>
                                                        <td>{{$row->jam_masuk}} - {{$row->jam_selesai}}</td>
                                                        <td>
                                                            <span>{{$row->mata_kuliah}}</span>
                                                        </td>
                                                        <td>{{$row->sks}}</td>
                                                        <td>{{$row->dosen}}</td>
                                                        <td>{{$row->ruangan}}</td>
                                                        <td class=" showme">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <button class="btn btn-warning rounded-circle text-warning bg-transparent" data-toggle="tooltip">
                                                                        <a href="{{ url('/jadwal/'. $row->id . '/edit') }}"><i class="fa fa-pencil text-warning" aria-hidden="true"></i> </a>
                                                                    </button>
                                                                </div>
                                                                <div class="col-sm-6" style="margin-left: -20px">
                                                                    <form action="{{ url('/jadwal', $row->id) }}" method="POST">
                                                                        @method('DELETE')
                                                                        @csrf                                            
                                                                        <button type="submit" class="btn btn-danger rounded-circle bg-transparent"><i class="fa fa-trash text-danger" aria-hidden="true"></i> </button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <script>
                                                                $(document).ready(function() {
                                                                    $('#hover').click(function() {
                                                                        $('#show-hide').toggle(500);
                                                                    });
                                                                });
                                                            </script>
                                                        </form>
                                                        </td>
                                                    </tr>    
                                                @else                   
                                                    <tr class="showhim">
                                                        <td>{{$row->hari}}</td>
                                                        <td>{{$row->jam_masuk}} - {{$row->jam_selesai}}</td>
                                                        <td>
                                                            <span>{{$row->mata_kuliah}}</span>
                                                        </td>
                                                        <td>{{$row->sks}}</td>
                                                        <td>{{$row->dosen}}</td>
                                                        <td>{{$row->ruangan}}</td>
                                                        <td class=" showme">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <button class="btn btn-warning rounded-circle text-warning bg-transparent" data-toggle="tooltip">
                                                                        <a href="{{ url('/jadwal/'. $row->id . '/edit') }}"><i class="fa fa-pencil text-warning" aria-hidden="true"></i> </a>
                                                                    </button>
                                                                </div>
                                                                <div class="col-sm-6" style="margin-left: -20px">
                                                                    <form action="{{ url('/jadwal', $row->id) }}" method="POST">
                                                                        @method('DELETE')
                                                                        @csrf                                            
                                                                        <button type="submit" class="btn btn-danger rounded-circle bg-transparent"><i class="fa fa-trash text-danger" aria-hidden="true"></i> </button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <script>
                                                                $(document).ready(function() {
                                                                    $('#hover').click(function() {
                                                                        $('#show-hide').toggle(500);
                                                                    });
                                                                });
                                                            </script>
                                                        </form>
                                                        </td>
                                                    </tr>
                                                @endif
                                                @endif
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- End Table mata kuliah -->    

<!-- table piket -->
    <div class="container-fluid mt-3 mb-5">
        <div class="row">            
            <div class="col-xl-12 col-lg-7">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card h-100">
                            <div class="card-body p-4">
                                <div class="media pt-2 pb-4" data-target="#ModalCenterInfo" data-toggle="modal">
                                    <img width="35" src="img/SVG/ic_picket.svg" class="mr-3 rounded-circle mt-n2">                                    
                                        <div class="media-body">
                                            <h6 class="font-weight-bold text-success" style="font-size: 18px;">Piket hari ini</h6>
                                        </div>
                                        <span class="text-muted{"> {{ Carbon\Carbon::now()->translatedFormat('l d F Y')}} </span>
                                    </div> 
                                </div>
                                <div class="activity">
                                    <div class="table-responsive ">                                        
                                        <table class="table table-xs mb-0 m-0">
                                                <thead>
                                                    <tr class="mr-3">
                                                        <th>Senin</th>                                                        
                                                        <th>Selasa</th>
                                                        <th>Rabu</th>
                                                        <th>Kamis</th>
                                                        <th>Jumat</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            @foreach ($piket_senin as $row)                                                            
                                                            <table class="per_days" style="width:100%;">                                                                                                                                                             
                                                                <tr>
                                                                    <td>{{ $row->nama }}</td>
                                                                </tr>
                                                                @if (empty($row->tugas))
                                                                    <tr class="table-empty">
                                                                        <td>Tidak ada tugas</td>
                                                                    </tr>
                                                                @else
                                                                    <tr>
                                                                        <td>{{ $row->tugas }}</td>
                                                                    </tr>
                                                                @endif
                                                            </table>
                                                            @endforeach
                                                        </td>                                                        
                                                        <td>
                                                            @foreach ($piket_selasa as $row)
                                                            <table class="per_days" style="width:100%;">
                                                                <tr>
                                                                    <td>{{ $row->nama }}</td>                                          
                                                                </tr>
                                                                @if (empty($row->tugas))
                                                                    <tr class="table-empty">
                                                                        <td>Tidak ada tugas</td>
                                                                    </tr>
                                                                @else
                                                                    <tr >
                                                                        <td>{{ $row->tugas }}</td>
                                                                    </tr>
                                                                @endif
                                                            </table>
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            @foreach ($piket_rabu as $row)
                                                            <table class="per_days" style="width:100%;">
                                                                <tr>
                                                                    <td>{{ $row->nama }}</td>                                          
                                                                </tr>
                                                                 @if (empty($row->tugas))
                                                                    <tr class="table-empty">
                                                                        <td>Tidak ada tugas</td>
                                                                    </tr>
                                                                @else
                                                                    <tr >
                                                                        <td>{{ $row->tugas }}</td>
                                                                    </tr>
                                                                @endif
                                                            </table>
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            @foreach ($piket_kamis as $row)
                                                            <table class="per_days" style="width:100%;">
                                                                <tr>
                                                                    <td>{{ $row->nama }}</td>                                          
                                                                </tr>
                                                                 @if (empty($row->tugas))
                                                                    <tr class="table-empty">
                                                                        <td>Tidak ada tugas</td>
                                                                    </tr>
                                                                @else
                                                                    <tr >
                                                                        <td>{{ $row->tugas }}</td>
                                                                    </tr>
                                                                @endif
                                                            </table>
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            @foreach ($piket_jumat as $row)
                                                            <table class="per_days" style="width:100%;">
                                                                <tr>
                                                                    <td>{{ $row->nama }}</td>                                          
                                                                </tr>
                                                                 @if (empty($row->tugas))
                                                                    <tr class="table-empty">
                                                                        <td>Tidak ada tugas</td>
                                                                    </tr>
                                                                @else
                                                                    <tr >
                                                                        <td>{{ $row->tugas }}</td>
                                                                    </tr>
                                                                @endif
                                                            </table>
                                                            @endforeach
                                                        </td>
                                                    </tr>
                                            </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
<!-- End Table Piket -->

@endsection