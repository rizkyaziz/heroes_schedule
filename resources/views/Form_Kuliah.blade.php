    @extends('template.template')

    @section('content')        

    @if (session('success'))
        <div class="container">
            <div class="alert alert-success">
            <p>{{ session('success') }}</p>
            </div>
        </div>
    @endif
    @if (session('error'))
        <div class="container">
            <div class="alert alert-error">
                <p>{{ session('error') }}</p>
            </div>
        </div>
    @endif

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-12 col-md-1">
                <div class="card o-hidden border-0 shadow-lg">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-12 mt-5">
                                <div class="text-center">
                                     <h1 class="h4 text-gray-900 mb-4">Jadwal Mata Kuliah</h1>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="px-5 py-4">
                                    <form action="{{ url('/jadwal/'. @$jadwal_kuliah->id) }}" method="POST">
                                        <div class="row">
                                            <div class="col-sm-6"> 
                                                
                                                @csrf

                                                @if(!empty(@$jadwal_kuliah))
                                                <input type="hidden" name="_method" value="PUT">                                                    
                                                @endif
                                                
                                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                <div class="form-group">
                                                    <label for="">Mata Kuliah</label>
                                                    <input type="text" class="form-control" name="mata_kuliah" id="" aria-describedby="helpId" required
                                                        placeholder="Nama mata kuliah ..." value="{{ old('mata_kuliah', @$jadwal_kuliah->mata_kuliah) }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Hari</label>
                                                    <select class="form-control" name="hari" id=""
                                                        aria-placeholder="When did you start class..." required>
                                                        <option disabled>When did you start class...</option>
                                                        {{-- <option value="">{{$jadwal_kuliah->hari}}</option> --}}
                                                        <option {{@$jadwal_kuliah->hari == 'Senin' ? 'selected' : ''}} value="Senin">Senin</option>
                                                        <option {{@$jadwal_kuliah->hari == 'Selasa' ? 'selected' : ''}} value="Selasa">Selasa</option>
                                                        <option {{@$jadwal_kuliah->hari == 'Rabu' ? 'selected' : ''}} value="Rabu">Rabu</option>
                                                        <option {{@$jadwal_kuliah->hari == 'Kamis' ? 'selected' : ''}} value="Kamis">Kamis</option>
                                                        <option {{@$jadwal_kuliah->hari == 'Jumat' ? 'selected' : ''}} value="Jumat">Jumat</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Dosen</label>
                                                    <input type="text" class="form-control" name="dosen" id="" aria-describedby="helpId"
                                                    placeholder="Siapa nama dosenmu ?" required value="{{ old('dosen', @$jadwal_kuliah->dosen) }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-6 mb3 mb-sm-0">
                                                        <label for="">Jam Masuk</label>
                                                        <div class="input-group clockpicker">
                                                            <input type="text" class="form-control" name="jam_masuk" value="{{ old('jam_masuk', @$jadwal_kuliah->jam_masuk) }}" required> <span
                                                                class="input-group-append"><span class="input-group-text"><i
                                                                class="fa fa-clock-o"></i></span></span>
                                                        </div>                                                        
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label for="">Jam Selesai</label>
                                                        <div class="input-group clockpicker">
                                                            <input type="text" class="form-control" name="jam_selesai" value="{{ old('jam_selesai', @$jadwal_kuliah->jam_selesai) }}" required> <span
                                                                class="input-group-append"><span class="input-group-text"><i
                                                                        class="fa fa-clock-o"></i></span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">SKS</label>
                                                    <input type="number" name="sks" id="" class="form-control form-control-user" required min="1" placeholder="Jumlah sks ..."
                                                        value="{{ old('sks', @$jadwal_kuliah->sks) }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Ruang</label>
                                                    <input type="text" class="form-control form-control-user"
                                                        placeholder="Dimana ruangan nya ?" name="ruangan" required value="{{ old('ruangan', @$jadwal_kuliah->ruangan) }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-3 mb-5 mr-1 float-right">                                       
                                            <a href="{{ url('jadwal')}}" class="nav-link px-5">Kembali</a>
                                            <button type="submit" class="btn btn-success px-5" id="next">Simpan</button>
                                        </div>                                        
                                    </form>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>

    <script src="{{ asset('../plugins/common/common.min.js')}}"></script>
    <script src="{{ asset('../plugins/moment/moment.js')}}"></script>
    <script src="{{ asset('../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{ asset('../js/plugins-init/form-pickers-init.js')}}"></script>
    @endsection